from django.urls import include, path
from ..views.task import TaskViewSet
from rest_framework import routers

task_routes = routers.SimpleRouter()
task_routes.register(
    r'tasks',
    TaskViewSet,
    basename='tasks'
)

urlpatterns = [
    path('api/', include(task_routes.urls))
]