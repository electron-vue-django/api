from django.db import models
from list.models import List

class Task(models.Model):
    title = models.CharField(blank=False, max_length=50)
    rank = models.IntegerField(blank=False)
    desc = models.TextField(blank=True)
    list = models.ForeignKey(List, null=True, related_name="tasks", on_delete=models.CASCADE)

    class Meta:
        app_label = "task"
        verbose_name = "task"
        verbose_name_plural = "Tasks"
