from table.models import Table
from list.serializers import ListSerializer
from rest_framework import serializers

class TableSerializer(serializers.ModelSerializer):
    lists = ListSerializer(many=True, read_only=True)

    class Meta:
        model = Table
        fields = '__all__'