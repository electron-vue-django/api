from django import views
from rest_framework import viewsets, permissions
from account_user.models.user import User
from table.models import Table
from table.serializers import TableSerializer
from django.forms import ValidationError
from rest_framework.response import Response
from rest_framework import status
from rest_framework.authtoken.models import Token



class TableViewSet(viewsets.ModelViewSet):
    queryset = Table.objects.all()
    serializer_class = TableSerializer
    permission_classes = [permissions.IsAuthenticated, permissions.IsAdminUser]


class TableOwnViewSet(viewsets.ModelViewSet):
    # queryset = Table.objects.all()
    serializer_class = TableSerializer
    permission_classes = [permissions.IsAuthenticated]

    def get_queryset(self):
        if self.request.user.is_authenticated and self.request.user.id :
            return Table.objects.filter(user=self.request.user.id)
        raise ValidationError({"not_authenticated": ["You're not connected"]})

    def create(self, request):
        # user_token = Token.objects.get(user=request.user)
        # user_id = Token.objects.get(key=user_token).user.pk
        request.data._mutable=True
        # request.data['user'] = user_id
        request.data['user'] = request.user.id
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        self.perform_create(serializer)
        headers = self.get_success_headers(serializer.data)
        return Response(serializer.data, status=status.HTTP_201_CREATED, headers=headers)