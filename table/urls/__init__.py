from django.urls import include, path
from ..views.table import TableViewSet, TableOwnViewSet
from rest_framework import routers

table_routes = routers.SimpleRouter()
table_routes.register(
    r'own',
    TableOwnViewSet,
    basename='tables'
)

table_routes.register(
    r'',
    TableViewSet,
    basename='tables'
)

urlpatterns = [
    path('api/tables/', include(table_routes.urls))
]