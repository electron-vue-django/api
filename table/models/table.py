from django.db import models
from django.utils import timezone

from account_user.models.user import User

class Table(models.Model):
    title = models.CharField(blank=False, max_length=50)
    user = models.ForeignKey(User, null=False, on_delete=models.CASCADE)

    class Meta:
        app_label = "table"
        verbose_name = "table"
        verbose_name_plural = "Tables"

    def __str__(self):
        return '%s' % (self.title)