from django.views.generic import TemplateView

class EmailVerification(TemplateView):
   template_name = 'email.html'