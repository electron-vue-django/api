from django.urls import include, path
from ..views.list import ListViewSet
from ..views.list import FindListByTableViewSet
from rest_framework import routers

list_routes = routers.SimpleRouter()
list_routes.register(
    r'lists',
    ListViewSet,
    basename='lists'
)

list_routes.register(
    r'lists/table',
    FindListByTableViewSet,
    basename='List by table'
)

urlpatterns = [
    path('api/', include(list_routes.urls))
]