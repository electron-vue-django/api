from django import views
from rest_framework import viewsets, permissions
from account_user.models.user import User
from list.models import List
from list.serializers import ListSerializer
from list.serializers import ListChoiceSerializer
from django.forms import ValidationError
from rest_framework.response import Response
from django.shortcuts import get_object_or_404
# from rest_framework import status

class ListViewSet(viewsets.ModelViewSet):
    queryset = List.objects.all()
    serializer_class = ListSerializer
    permission_classes = [permissions.IsAuthenticated]


class FindListByTableViewSet(viewsets.ViewSet):
    permission_classes = [permissions.IsAuthenticated]
    
    def list(self, request):
        pass

    def create(self, request):
        pass

    def retrieve(self, request, pk=None):
        user = get_object_or_404(User, pk=pk)
        self.check_object_permissions(request, user)
        table_id = pk
        if table_id: 
            queryset = List.objects.only("id", "title").filter(table_id=table_id)
            serializer = ListChoiceSerializer(queryset, many=True)
            return Response(serializer.data)
        raise ValidationError({"error": "No id provided"})

    def update(self, request, pk=None):
        pass

    def partial_update(self, request, pk=None):
        pass

    def destroy(self, request, pk=None):
        pass

    def view(self, request):
        pass