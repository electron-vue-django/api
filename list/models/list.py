from table.models import Table
from django.db import models

class List(models.Model):
    title = models.CharField(blank=False, max_length=50)
    table = models.ForeignKey(Table, null=True, related_name="lists", on_delete=models.CASCADE)

    class Meta:
        app_label = "list"
        verbose_name = "list"
        verbose_name_plural = "Lists"

    def __str__(self):
        return "%s" % (
            self.title        
        )