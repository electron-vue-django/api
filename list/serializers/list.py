from list.models import List
from rest_framework import serializers
from task.serializers import TaskSerializer


class ListSerializer(serializers.ModelSerializer):
    tasks = TaskSerializer(many=True, read_only=True)

    class Meta:
        model = List
        fields = ['id', 'title', 'table', 'tasks']


class ListChoiceSerializer(serializers.ModelSerializer):

    class Meta:
        model = List
        fields = ['id', 'title', 'table']
