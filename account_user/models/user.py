from django.contrib.auth.models import AbstractUser
from django.db import models
from django.contrib import admin
# from datetime import date


class User(AbstractUser):
    email = models.EmailField(blank=False)
    # date_joined = date.today()

    class Meta:
        app_label = "account_user"
        verbose_name = "User"
        verbose_name_plural = "Users"
        # permissions = [('IsAuthenticated', 'Can generate token')]


admin.site.register(User)