# '/api/auth' use rest_auth package to authenticate users

from django.urls import include, path, re_path
from ..views.user import UserViewSet
from ..views.user import UserDetailsCheckToken
from rest_framework import routers
from dj_rest_auth import urls as auth_urls
from templates.email_confirm import EmailVerification
from allauth.account.views import signup, confirm_email, email_verification_sent, login
from dj_rest_auth.registration.views import  RegisterView

user_routes = routers.SimpleRouter()
user_routes.register(
    r'user',
    UserViewSet,
    basename='user',
)
user_routes.register(
    # check token
    r'check',
    UserDetailsCheckToken,
    basename='user-token',
)

urlpatterns = [
    path('api/auth/', include(user_routes.urls)),
    path('api/auth/', include(auth_urls)),
    path('api/auth/register/', RegisterView.as_view(), name='account_signup'),
    path("api/auth/accounts/confirm-email/", email_verification_sent, name="account_email_verification_sent"),
    re_path(r"^api/auth/accounts/confirm-email/(?P<key>[-:\w]+)/$",confirm_email, name="account_confirm_email"),
    path('api/auth/email/success/', EmailVerification.as_view(), name='account_email_success'),

    # api/auth/accounts/ signup/ [name='account_signup']
    # api/auth/accounts/ login/ [name='account_login']
    # api/auth/accounts/ logout/ [name='account_logout']
    # api/auth/accounts/ password/change/ [name='account_change_password']
    # api/auth/accounts/ password/set/ [name='account_set_password']
    # api/auth/accounts/ inactive/ [name='account_inactive']
    # api/auth/accounts/ email/ [name='account_email']
    # api/auth/accounts/ confirm-email/ [name='account_email_verification_sent']
    # api/auth/accounts/ ^confirm-email/(?P<key>[-:\w]+)/$ [name='account_confirm_email']
    # api/auth/accounts/ password/reset/ [name='account_reset_password']
    # api/auth/accounts/ password/reset/done/ [name='account_reset_password_done']
    # api/auth/accounts/ ^password/reset/key/(?P<uidb36>[0-9A-Za-z]+)-(?P<key>.+)/$ [name='account_reset_password_from_key']
    # api/auth/accounts/ password/reset/key/done/ [name='account_reset_password_from_key_done']
    # api/auth/accounts/ social/
]
