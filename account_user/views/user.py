from rest_framework import viewsets, permissions
from account_user.models import User
from account_user.serializers import UserSerializer
from rest_framework.authtoken.models import Token

from electron_api.exception.customException import CustomException
from rest_framework import status


class UserViewSet(viewsets.ModelViewSet):
    queryset = User.objects.all()
    serializer_class = UserSerializer
    search_fields = ('username', 'email')
    permission_classes=[permissions.IsAuthenticated, permissions.IsAdminUser]
    

class UserDetailsCheckToken(viewsets.ModelViewSet):
    serializer_class = UserSerializer
    search_fields = ('username', 'email')
    permission_classes=[permissions.AllowAny]

    def get_queryset(self):
        if self.request.user.is_authenticated :
            user_token = Token.objects.get_or_create(user=self.request.user)
            user = Token.objects.get(key=user_token[0]).user.pk
            if user_token and user:
                return User.objects.only("id", "username").filter(pk=user)
        raise CustomException(detail=
            {"not_authenticated": ["You're not connected"]}, 
            status_code=status.HTTP_400_BAD_REQUEST
        )
