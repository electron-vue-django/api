from rest_framework import serializers
from account_user.models import User
from django.contrib.auth.hashers import make_password

from electron_api.exception.customException import CustomException
from rest_framework import status

# from rest_framework.permissions import IsAuthenticated


class UserSerializer(serializers.ModelSerializer):
    # permission_classes = (IsAuthenticated,)
    password = serializers.CharField(style={'input_type': 'password'})
    password_confirm = serializers.CharField(
        write_only=True,
        style={'input_type': 'password'}
    )

    def create(self, validated_data):
        if(validated_data['password_confirm'] == validated_data['password']):
            validated_data['password'] = make_password(validated_data['password'])
            # Remove password_confirm before saving
            validated_data.pop('password_confirm')
            return super().create(validated_data)
        else:
            raise CustomException(detail=
                {"password_confirm": ["Passwords are not identical"]}, 
                status_code=status.HTTP_400_BAD_REQUEST
            )

    class Meta:
        model = User
        fields = "__all__"
